# EURO-LABS CERN Transnational Access

<center>

![EURO-LABS Logo ](eulabs-cern-logo.png)

</center>

[CERN](http://cern.ch) participates in the [EURO-LABS](http://web.infn.it/EURO-LABS) project offering **Trans-National Access (TNA)** to the facilities below which cover the full spectrum of active physics research in the Organization.

<center>

| Nuclear Physics | Accelerator R&D | HEP Detector R&D |
|:---------------:|:---------------:|:----------------:|
| ISOLDE | HiRadMat | PS Test Beams |
| nTOF | CLEAR | SPS Test beams |
|  |XBOX | IRRAD | 
| | | GIF++ |

</center>

The general framework of the programme and information on the trans-national access in all participating **Research Infrastructures (RI)** within EURO-LABS is available in the project web [http://web.infn.it/EURO-LABS](https://web.infn.it/EURO-LABS).  

In this site, the specific implementation for the CERN facilities is provided.
