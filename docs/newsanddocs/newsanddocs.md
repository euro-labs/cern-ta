# News & Documents 

## Events

- 3rd EURO-LABS Annual Meeting - [CERN, 28-30 October 2024](https://indico.cern.ch/e/euro-labs-tam)
- Advanced Training School on Operation of Accelerators - [CERN, 3-7 June 2024](https://indico.cern.ch/e/ATSOA-CERN24) 

## Documents - publications

- IPAC2024 Nashville May 19-24, 2024 - EURO-LABS poster by I.Efthymioulos [pdf](./EURO-LABS-IPAC24-Poster_IE.pdf){:download="EURO-LABS-IPAC24-Poster_IE.pdf}
