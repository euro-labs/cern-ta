# Frequently Asked Questions

??? question "can CERN reimburse the home institute?"

    This is not possible. EURO-LABS CERN supports visits to the facilities and covers either 
    
    - the full trip via an official CERN travel request, or 
    - provides a **supplementary** daily allowence to cover living expenses in the area (the case for WP4 test beam users)
    <p> 
    In the first case, the full travel costs are covered within the allowed limits from the CERN rules (link here). In the second, the visitors can receive additional payments for their expences (for example to cover travel or hotel costs). The payments are done directly to the user, through the official CERN travel or allowance claims. 

??? question "reimbursment delay"

    CERN would reimburse the agreed amount at the end of the travel, certifying the stay at CERN. Exceptionally for the PS&SPS Test beam users, the process can start before the end of the travel, provided all the required papers have been received. 
      
