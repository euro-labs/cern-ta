
# EURO-LABS CERN - TA Templates

## TA Application forms

1.  Project description form [EURO-LABS-TA-Application-Form.docx](https://cernbox.cern.ch/s/uF2Yn9iwv9K1jDM){:download="EURO-LABS-TA-Application-Form.docx"} valid for all facilities.

2.  Project and user team information form (excel file)

    **General EURO-LABS template** 
    
    [EURO-LABS-TA-ApplicationData-General.xlsx](https://cernbox.cern.ch/s/kwtr2dAXmInQaRQ){:download="EURO-LABS-TA-ApplicationData-General.xlsx}

    ---
    **And CERN-specific templates**

    | Facility | Template file (excel) |
    | :-----------: | :-----------: |
    | nTOF | [EURO-LABS-TA-ApplicationData-CERN-nTOF.xlsx](https://cernbox.cern.ch/s/Xiocf8VOOHpTfPx){:download="EURO-LABS-TA-ApplicationData-CERN-nTOF.xlsx"}|
    | PS-SPS test beams | [EURO-LABS-TA-ApplicationData-CERN-PSSPS.xlsx](https://cernbox.cern.ch/s/9o5tK24RQsytcEo){:download="EURO-LABS-TA-ApplicationData-CERN-PSSPS.xlsx"} |
    | IRRAD | [EURO-LABS-TA-ApplicationData-CERN-IRRAD.xlsx](https://cernbox.cern.ch/s/c98yqsOT6ZGlXUp){:download="EURO-LABS-TA-ApplicationData-CERN-IRRAD.xlsx"} |
    | GIF++ | [EURO-LABS-TA-ApplicationData-CERN-GIFpp.xlsx](https://cernbox.cern.ch/s/epaMfp7FKKEh40i){:download="EURO-LABS-TA-ApplicationData-CERN-GIFpp.xlsx"} |
    | HiRadMat | [EURO-LABS-TA-ApplicationData-CERN-HRMT.xlsx](https://cernbox.cern.ch/s/73ziZ7KQ57Dt5uy){:download="EURO-LABS-TA-ApplicationData-CERN-HRMT.xlsx"} |
    | CLEAR | [EURO-LABS-TA-ApplicationData-CERN-CLEAR.xlsx](https://cernbox.cern.ch/s/0viQFKtgT0c5Qph){:download="EURO-LABS-TA-ApplicationData-CERN-CLEAR.xlsx"} |

## TA Project or Experiment Report

- Project or experiment report form as word or excel (preferred format) valid for all facilities:
    - [EURO-LABS-TA-ExperimentReport.docx, word file](https://cernbox.cern.ch/s/QqBfRVpRoHoljeA){:download="EURO-LABS-TA-ExperimentReport.docx} 
    - [EURO-LABS-TA-ExperimentReport.xlsx, excel file](https://cernbox.cern.ch/s/okrwi0hgy6c3Mim){:download="EURO-LABS-TA-ExperimentReport.xlsx}

## TA Project Publication List 

To be completed by the Facility Coordinators per reporting period

- [EURO-LABS-FacilityPublicationList.xlsx, excel file](https://cernbox.cern.ch/s/zPG62RcaKomKusx){:download="EURO-LABS-FacilityPublicationList.xlsx"}


## Presentation template

- [ieEURO-LABS-slides-template.potx](https://cernbox.cern.ch/s/GkI5FJeRXAXiGz5){:download="ieEURO-LABS-slides-template.potx"}