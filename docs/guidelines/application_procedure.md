# Trans-national Access Application Procedure

The TA cycle involves several actors:

- The **Group Leader or Spokesperson or PI** who is applying for EURO-LABS funds for the experiment/project and the team members **Users**.
- The **Coordinator** of the facility.
- The **Administrative Assistant** for the facility.

and generally has two phases:

- **Initial**: before the experiment takes place.
- **Final**: at the start or soonafter the completion of the experiment. 

The diagram below provides a generic flowchart of the steps involded, while specific information per facility are available in the corresponding links. 

### Initial phase

```mermaid
%%{ init: {
    'sequenceDiagram': { 'curve': 'stepBefore' },
    'theme': 'base',
    'themeVariables':
        { 'fontSize': '12px', 'fontFamily': 'Arial, sans-serif'}
} }%%
sequenceDiagram
    # title EURO-LABS Trans-national Access - initial phase
    autonumber
    actor U as User
    participant G as Group Leader
    # participant F as Facility Coordinator
    # participant S as Administrative Assistant
    participant FS as Facility Coordinator <br/> or <br/> Administrative Assistant
    
    rect rgb(200, 230, 255)
    G->>+FS: Application documents: <br/> application form with project description (word file), and user team data (excel file)
    Note right of FS : verify eligibility
    Note right of FS : scienticif evaluation - USP   
    FS->>-G: information on USP approval
    FS->>+G: initial documents to return signed: invitation letters, user registration forms
    G-->>+U: distributes documents to users
    U-->>-FS : upload signed documents
    # Note left of G : distributes documents <br/> to team members
    # G->>-FS: collects and uploads signed documents
    end
```

- **Application documents**: application form with project description (Word document), and user information data (excel file), templates available [here](./application_templates.md). 
- **Initial documents to sign and return** : invitation letters for each user, and CERN user registration forms in case it is required. 


### User Selection Panel

The EURO-LABS TA applications will be reviewed by the appropriate EURO-LABS **User selection Panel (USP)**. The USP evaluates the proposals based on scientific merit (usualy granted if the experiment is already included in the yearly beam schedule of the facility), [eligibility criteria](https://web.infn.it/EURO-LABS/eligibility-criteria/), and available funding.

!!! note 

    The USP does not control the allocation of the beam time and the experimental program in the facilities. At CERN this is done by the Physics Coordinators and the yearly program is endorsed by the CERN Scientific Committees. 

The EURO-LABS transnational access comes as **supplementary** support to the teams for their visits to CERN. 

The TA applications can arrive anytime.  If the project is already approved and included in the facility schedule, the application will be considered directly. Otherwise, it will be put on hold by the facility coordinator, and activated after the project has been evaluated by the scientific committees and is included in the beamtime schedule of the facility.

!!! note 

    For the PS & SPS Test beams, the applications will be reviewed in beam periods as announced in the Users' Meeting. 

### Final phase

```mermaid
sequenceDiagram
    # title : EURO-LABS Trans-national Access - Final Phase
    autonumber
    participant G as Group Leader
    participant FS as Facility Coordinator <br/>or <br/>Administrative Assistant

    rect rgb(255, 230, 230)
    
    G->>+FS : provides list of actual dates of each team member in the facility
    Note right of FS : process information
    FS->>-G : provides final documents to return signed: group and user presence in the facility
    activate G
    Note left of G : distributes documents <br/> to team members
    G->>-FS : collects and uploads signed documents: <br/> group and user presence declarations, user IDs, bank coordinates
    loop
        FS->FS : forwards documents to CERN admin <br/> for reimburesement
    end
    #activate FS:
    FS->>G : payments to user's bank accounts 
    end
```
- **Final documents: to sign and return**: 

    - beam confirmation time for the group, for the days in the facility for each member receiving financial support, to be signed by the group leader
    - declaration of honour for each member receiving financial support for the days spent at the facility. 
    
    In addtion, each team member will be asked to provide through EDH, information of a bank account where the money will be transferred. 

All documents must be uploaded to the designated CERNBOX folder by the Facility Coordinator, with filenames that include the user's name and the project identifier.