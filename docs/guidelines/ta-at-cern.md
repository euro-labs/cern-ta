
# EURO-LABS at CERN - Transnational Access

## Modality of access

CERN provides **free of charge** beam time in the participating [facilities](../cern_facilities.md) and additional **financial support**, to selected research teams (**user-groups**) from academia and research institutes to perform their experiments. 

The experiment needs to be approved by the Physics Program Coordinator of the facility and included in the yearly approved beam-time or operation schedule, endorsed by the corresponding CERN scientific committees to be eligible for EURO-LABS support. 

The access can be of two types:

**Transnational Access (TA)**  when the team visits the infrastructure to make use or it. 

!!! info 

    Detailed information on the financial support at CERN for TA in this [Financial Support](./travelcosts.md) on the right.
         
**Remote Trans-national Access (RA)** through the provision of scientific services to the users without a visit to the facility. 

??? example

    testing of user equipment sent to the facility, irradiation and/or analysis of material samples, <br/> remote access to installed experiments for special data-taking. 
   
## User support under TNA
 
The EURO-LABS funds can be used for: 

- {== financial support to the team members ==} during their stay at CERN (TA access): subsistence and travel expenses.
- {== expenses related to the expriment==}: installation and/or transport of the equipment.
- {== contributing to operating costs==}: consumables, adaptation of infrastructure.

## Application procedure

The cycle of a TA application has two phases:

- **initial**: before the experiment takes place.
- **final**: at the start or soon after the completion of the experiment. 

The steps to follow in each phase are detailed in the [Application procedure](./application_procedure.md) tab. 

### End of Experiment Report

Within **two weeks** from the completion of the experiment, the **Group Leader/PI** must provide a short report (template [here](./application_templates.md)) summarizing the objectives and achievements of the experiment, and send it to the Facility Coordinator.

The **Group Leader/PI** should make sure that EURO-LABS is properly acknowledged using the text below in any publication of presentation by the group or team members.

![](../images/EURO-LABS_reftext.jpg)