# Travel Expenses Reimbursement within EURO-LABS

The TA financial support of the selected users for the visits to CERN follows the general guidelines for travel reimburesment of CERN visitors, outlined below.[^1].

[^1]: [Travel to CERN](https://cds.cern.ch/record/2130356/files/Travels%20to%20CERN%20documentation.pdf)

# Rules and Principles

CERN will reimburse the costs of travel when they correspond to the use of the most cost-efficient and suitable
route and means of transport for the date of travel, from the place of residence or of employment of the
traveller.

Public transport should be used to the extent possible.

CERN makes reference to a daily travel allowance (DTA) when processing reimbursement requests to compensate the
cost of accommodation, meals and miscellaneous expenses.

<!--Any reimbursements of expenses exceeding the limits mentioned hereunder will necessitate the specific
authorization of the Director-General.-->

### Transport

CERN shall pay the transport expenses for a return journey between the place of residence or of employment of
the traveller and CERN <!--, or any other place where the person is invited by CERN.-->

The following classes of travel are allowed:

- Air plane: economy or equivalent (including one piece of luggage); tariffs like “Economy Premium” are not
considered as being equivalent;

- Train: first-class, sleeper or couchette;

- Rental car: compact or economy category

Rental car or taxi is paid or reimbursed only if necessary and duly justified (e.g. bulky equipment to be transported, unsuitable public transport,…). Rental car in Geneva area is not reimbursed. 

Transport costs between the airport / train station and CERN, or any other final destination, are reimbursed at actual cost provided each individual item claimed exceeds 20 CHF.

The ceiling for the transpor costs within EURO-LABS is defined to **400 CHF** for Continental and to **1250 CHF** for trans-Continental destinations. This covers tickets on the travel class mentioned above. Any additions (like Premium or Business or equivalent) should be bought separately and won't be reimbursed. 

For the **PS and SPS Test Beams** TA, due to increased number of applications, no travel or transport costs to CERN will be covered. 

<!-- Where the traveller uses his own means of transport (e.g. private car) or a travel class different than these
mentioned above, CERN will reimburse the value of the cheapest ticket available on allowed modes of transport. -->

## Accommodation, meals and miscellaneous expenses
Accommodation, meals and miscellaneous expenses are paid when the place of residence of the traveller is more
than 50 km from CERN or the duty travel destination. Accommodation expenses can be reimbursed on the basis of real costs according to the rules.[^1] Approval by the Facility Coordinator is required, depending on the available budget. 

The DTA for EURO-LABS TA, **except** for the PS and SPS Test Beams TA (see below), is defined up to a ceiling of **145 CHF/night at CERN**, depending on budget availability, <!--- intended to compensate expenses for logging, meals and local transport --> and is paid as a lump sum.

For the PS & SPS Test Beam users, due to a very large number of applications received in 2023, a number of restrictions had to be applied. In particular:

- The per-diem has been reduced to **80 CHF/night**, with the Home Insitutes of the Users allowed to top-up.
- Exclusively students and post-graduates will be supported.
- The total number of DTA per project is capped (depending on the beam time duration of the project).

# Procedure

## Travel organisation

In case payment of the travel costs are authorised by the FC:

- users registered as externals, i.e non-members of CERN, organize and book their ticked on their means respecting the rules defined above,
- CERN users (MPA) should follow the usual travel procedures for CERN travels 

## Cost claim

At the end of the experiment the users should provide the signed documents described [here](groupleaders.md).

For the travel costs the users should provide in addition the scanned copies of receipts and tickets, within 6 months following the return date. After
this deadline, no reimbursement can be made.

Payments are made by bank transfer to the bank account of the traveller in CHF. Cash payments are not
authorised. 



