# Guidelines for Applicants

Information for Group Leaders or Spokespersons of team wishing to receive EURO-LABS TA Support

## Group Leaders / PI

1. **Initial** Start by contacting the **Facility Coordinator (FC)** to discuss the technical aspects and feasibility of the project and the eligibility of the your user group for EURO-LABS funds.

    * list of CERN facility coordinators [here](../cern_facilities.md) </p>
    * eligibility criteria for projects can be found [here](https://web.infn.it/EURO-LABS/eligibility-criteria/))

    Your experiment must appear in the yearly schedule of the facility to be eligible for EURO-LABS funds.

2. **Application** Download, complete, and return the EURO-LABS TA application forms, i.e. the two documents described below: 

    - the project description form, providing a SHORT description of the experimet, its objectives along with the scientific background of the team related to the proposed experiment,
    - an excel file, providing the information for all group participants and the level of requested funds. 

        !!! tip

            In this initial phase, in the excel file only the columns up to and including the "TNA Request" need to be filled. 
            
            The available funds are limited, thus try to make reasonable requests and give priority to students or young researchers and gender balance. 

            Try to respect the proposed format of the document and don't change or modify its structure (columns). 

    While processing the application, you will receive documents to sign for all group members. Distribute them to your team and make sure they are timely signed and returned to the indicated upload location. 

    Details of the procedure and document flow at the [Applicatin procedure tab](./application_procedure.md).

3.	**Outcome of the User Selection Panel (USP)** The Facility Coordinator (FC) will contact you by email regarding the outcome of the User Selection Panel (USP) and the proposed allocation of funds. In case of disagreement, please contact the FC again for possible rectifications, within the available funds.

4. **End of experiment - reimbursement** Once the beam slot and travel details are finalized for all group members, you should inform the FC. 

    The recommended way is to fill in the excel file of the application the last columns under "Actual", and return it to the FC. Otherwise a simple e-mail or text file with the list of arrival/departure dates of each Group member is also acceptable. 

    !!! warning
        this should be done right after and not later than two weeks after the beam time at CERN, in order to trigger the administrative procedures for the reimburesements. Beyond that period no reimburesements will be considered.

5.	**TA experiment report**: When the experiment is completed, you will have to fill a TA report (template [here](expreport.md)) summarizing the objectives and achievements and send it to the facility coordinator within two weeks after completion of the project.

8.	**Publications**: The results will have to be published in a journal with the EURO-LABS acknowledgement. See below:

 ![](../images/EURO-LABS_reftext.jpg)

## Group Members

Group members would have to provide information and sign few documents : 

Before the visit : 
- An **Invitation Letter** describing:
    - the conditions of the required access. 
    - the financial support provided
    - requirement to publish results as open data

- If needed, the **training courses** to register and attend

At the start or at the end of the visit but not later than two weeks:

- The declaration of honour for the days of presence at CERN
- will be contacted by e-mail to provide ID information and bank account details for the reimbursement 

!!! note
    most of the documents discussed here are generated automatically from the information provided in the excel file accompanying the application. In case of errors an updated file need to be filled and send to the FC in order to re-generate the documents.
