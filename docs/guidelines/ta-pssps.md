
# EURO-LABS at CERN - Transnational Access PS-SPS Test Beams

Specific information for PS-SPS test beam TA applications

## Financial conditions

Due to a very large number of applications, the restrictions below are applied:

- The per-diem has been reduced to 80 CHF/night, with the Home Insitutes of the Users allowed to top-up.
- Exclusively students and post-graduates will be supported.
- The total number of Daily Travel Allowance (DTA) per project is capped (depending on the beam time duration of the project).


   !!! note 

      Users registered with >50% presence in the local area (i.e. resident in Switzerland or France) are not eligible for TA.


## Application deadlines

The TA applications will be handled in batches, with deadlines as announced at the User's meeting. 

For 2024 the deadlines are :
    
- July 31st, 2024 for test beam experiments in the first period of the year,
- October 31, 2024 for the second period of the year


    
    
    